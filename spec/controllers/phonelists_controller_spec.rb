require 'spec_helper'

describe PhonelistsController do

before(:each) do
	sign_in FactoryGirl.create(:user)
end

	describe "#phonelist" do
		it "create OK!" do
			@phone = FactoryGirl.create(:phone)
			@phonelist = FactoryGirl.create(:phonelist, namelist: "adit123", nomerlist: "123456", phone_id: @phone.id)
			@phone.phonelists << @phonelist
			@phone.phonelists.first.namelist.should  eq("adit123")
		end
		it "delete OK!" do
			@phone = FactoryGirl.create(:phone)
			@phonelist = FactoryGirl.create(:phonelist, namelist: "adit123", nomerlist: "123456", phone_id: @phone.id)
			@phone.phonelists << @phonelist
			expect{
					delete :destroy, id: @phonelist, phone_id: @phone.id
				}.to change(Phonelist, :count).by(-1)
		end
		it "update OK!" do
			@phone = FactoryGirl.create(:phone)
			@phonelist = FactoryGirl.create(:phonelist, namelist: "adit123", nomerlist: "123456", phone_id: @phone.id)
			@phone.phonelists << @phonelist
			put :update, id: @phonelist, phone_id: @phone.id, phonelist: FactoryGirl.attributes_for(:phonelist, namelist: "adit", nomerlist: "654321")
				@phone.reload
				@phone.phonelists.first.namelist.should eq("adit")
				@phone.phonelists.first.nomerlist.should eq("654321")
		end
	end
end