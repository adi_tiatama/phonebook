require 'spec_helper'

describe AccountsController do
	describe "when Administrator is logged in" do
		before(:each) do
			user_admin = FactoryGirl.create(:user_admin)
			sign_in :user, user_admin
		end

		context "Administrator??" do
			it "yes, this is Administrator bro" do
				subject.current_user.role.should eq("Administrator")
			end
			it "can delete user" do
				@user = FactoryGirl.create(:user)
				expect{
					delete :destroy, id: @user
				}.to change(User, :count).by(-1)
			end
			it "delete redirects to account" do
				@user = FactoryGirl.create(:user)
				delete :destroy, id: @user
				response.should redirect_to accounts_url
			end
			it "can update user" do
				@user = FactoryGirl.create(:user, email: "adi_tiatama@dwp.co.id", password: "1594826", role: "User")
				#binding.pry
				put :update, id: @user, account: FactoryGirl.attributes_for(:user, role: "Administrator")
				@user.reload
				@user.role.should eq("Administrator")
			end
		end
	end

	describe "when User is logged in" do
		before(:each) do
			user_biasa = FactoryGirl.create(:user)
			sign_in :user, user_biasa
		end

		context "Administrator??" do
			it "No, this is User bro" do
				subject.current_user.role.should eq("User")
			end
			it "can not delete user" do
				@user = FactoryGirl.create(:user)
				expect{
					delete :destroy, id: @user
				}.to_not change(User, :count).by(-1)
			end
			it "can not update user" do
				@user = FactoryGirl.create(:user)
				put :update, id: @user, account: FactoryGirl.attributes_for(:user, role: "Administrator")
				@user.reload
				@user.role.should_not eq("Administrator")
			end
		end
	end
end


