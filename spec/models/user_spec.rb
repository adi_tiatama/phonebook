require 'spec_helper'

describe User do
	it "1. valid factory" do
		FactoryGirl.create(:user).should be_valid
	end
	it "2. invalid without email" do
		FactoryGirl.build(:user, email:nil).should_not be_valid	
	end
	it "3. invalid without password" do
		FactoryGirl.build(:user, password:nil).should_not be_valid	
	end
	it "3. invalid without email & password" do
		FactoryGirl.build(:user, email:nil, password:nil).should_not be_valid	
	end
	it "4. does not valid, duplicates email" do
		FactoryGirl.create(:user, email: "1234@dwp.co.id")
		FactoryGirl.build(:user, email: "1234@dwp.co.id").should_not be_valid
	end
end