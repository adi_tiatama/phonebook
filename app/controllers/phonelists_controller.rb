class PhonelistsController < ApplicationController
  load_and_authorize_resource

	def create
		@phone = Phone.find(params[:phone_id])
		@phonelist = @phone.phonelists.create(phonelist_params)
		redirect_to phone_path(@phone)
	end
	def destroy
		@phone = Phone.find(params[:phone_id])
		@phonelist = @phone.phonelists.find(params[:id])
		@phonelist.destroy
		redirect_to phone_path(@phone)
	end
	def edit
        @phone = Phone.find(params[:phone_id])
        @phonelist = @phone.phonelists.find(params[:id])
  end
  def update
   	@phone = Phone.find(params[:phone_id])
	  @phonelist = @phone.phonelists.find(params[:id])

	  if @phonelist.update(params[:phonelist].permit(:namelist, :nomerlist))
			redirect_to @phone
		else
			render 'edit'
		end
  end


  private
    def phonelist_params
      params.require(:phonelist).permit(:namelist, :nomerlist)
   end

end
