class Phone < ActiveRecord::Base
  	belongs_to :user
	has_many	:phonelists, dependent: :destroy
	# validates :nomer, presence: true, uniqueness: { scope: :nomer, message: "should not duplicates"}
	# validates :name, presence: true, uniqueness: { scope: :name, message: "should not duplicates"}
  validates :nomer, presence: true
  validates :name, presence: true
end


