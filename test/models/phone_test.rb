require 'spec_helper'
require 'faker'
class PhoneTest < ActiveSupport::TestCase
=begin

rescue Exception => e

end
   test "the truth" do
     assert true
   end

  test "should not save post without title" do
    phone = Phone.new
    assert !phone.save
  end
=end


=begin
rescue Exception => e

end
  describe Phone do
    #it "order by name" do
      #tes1 = Phone.create!(name: "aditiatama", nomer: "085712345678")
      #tes2 = Phone.create!(name: "adit", nomer: "085787654321")

      #expect(Phone.ordered_by_name).to eq([tes1, tes2])
    it "1 fails validation with no name (using error_on)" do
      expect(Phone.new).to have(1).error_on(:name)
    end

    it "2 fails validation with no name (using errors_on)" do
      expect(Phone.new).to have(1).errors_on(:name)
    end

    it "3 fails validation with no name expecting a specific message" do
      expect(Phone.new.errors_on(:name)).to include("can't be blank")
    end

    it "4 fails validation with a short name (using a validation context)" do
      expect(Phone.new(:name => "too short")).
      to have(1).errors_on(:name, :context => :publication)
    end

    it "5 passes validation with a longer name (using a validation context)" do
      expect(Phone.new(:name => "a longer name")).
      to have(0).errors_on(:name, :context => :publication)
    end

    it "6 passes validation with a name & nomer (using 0)" do
      expect(Phone.new(:name => "adi tiatama", :nomer => "085712345678")).to have(0).errors_on(:name)
    end

    it "7 passes validation with a name (using :no)" do
      expect(Phone.new(:name => "adi tiatama", :nomer => "085712345678")).to have(:no).errors_on(:name)
    end
=end

 
describe Phone do 

  it "is invalid without a firstname" 
  it "is invalid without a lastname" 
  it "returns a contact's full name as a string" 
end 



end
